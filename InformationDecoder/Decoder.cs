﻿using BackgroundProcess;
using System;
using System.IO;
using System.IO.Compression;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace InformationDecoder
{
    class Decoder
    {
        // set max input buffer in input cmd
        const int READLINE_BUFFER_SIZE = 1000000;

        static void Main(string[] args)
        {
            Console.WriteLine("Paste the encoded string: ");

            string input = ReadBigLine();

            try { 
                Payload payload = (Payload) Decode(input);
                Console.WriteLine("\n*** Start of decoded object ***\n");
                Console.WriteLine(payload.ToString());
                Console.WriteLine("*** End of decoded object ***");
            } catch (Exception e)
            {
                Console.WriteLine("Could not decode input string: " + e.Message);
            }

            Console.WriteLine("Press any key to quit.");
            Console.ReadLine();
        }

        // we need this special function to read from STDIN
        // note: the normal function is limited to 256 chars of buffer
        private static string ReadBigLine()
        {
            Stream inputStream = Console.OpenStandardInput(READLINE_BUFFER_SIZE);
            byte[] bytes = new byte[READLINE_BUFFER_SIZE];
            int outputLength = inputStream.Read(bytes, 0, READLINE_BUFFER_SIZE);
            char[] chars = Encoding.UTF8.GetChars(bytes, 0, outputLength);
            return new string(chars);
        }

        static object Decode(String info)
        {
            // decode object from Base 64 string
            byte[] decoded = Convert.FromBase64String(info);

            // serialize & compress object
            using (var memoryStream = new MemoryStream(decoded))
            {
                using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Decompress))
                {
                    BinaryFormatter binaryFormatter = new BinaryFormatter();
                    return binaryFormatter.Deserialize(gZipStream);
                }
            }


        }
    }
}
